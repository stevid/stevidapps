//
//  SA_Page2.h
//  PhotoSend
//
//  Created by SA_ on 10/22/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SA_SpinnerView.h"
#import "SA_ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RNBlurModalView.h"
#import "SA_ActionFromMyAlertView.h"
#import <AVFoundation/AVFoundation.h>

@interface SA_Page2 : UIViewController <UIApplicationDelegate,CLLocationManagerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,OkManagementOnAlertRNBlurModalView,SA_ActionFromMyAlertView>

- (IBAction)didPressSpinnerViewButton:(id)sender;
- (IBAction)buttonOptionsAction:(id)sender;
- (IBAction)buttontest:(id)sender;
- (IBAction)seguente:(id)sender;

@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (weak, nonatomic) IBOutlet UIButton *buttonOpzioneAOutlet;

@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UIButton *buttonOpzioneCOutlet;
@property (strong,nonatomic) UIWindow *window;

@property (strong,nonatomic) CLLocationManager *locationManager;

@property (nonatomic, retain) UIImagePickerController *imagePicker;

- (IBAction)Fotocamera:(id)sender;

@end
