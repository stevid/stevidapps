//
//  SA_Page2.m
//  PhotoSend
//
//  Created by SA_ on 10/22/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_Page2.h"
#import "SA_SpinnerView.h"
#import "SA_ViewController.h"
#import "CoreLocation/CoreLocation.h"
#import "SA_AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "SA_ConfirmPageSendPhotoViewController.h"
#import "MyAlertViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "CustomAlertView.h"
#import "CXAlertView.h"

@interface SA_Page2 ()

@end

@implementation SA_Page2
UIAlertView *alert;
CXAlertView *modalViewAskTakePhoto;
CXAlertView *alertGPSNotActive;
CXAlertView *alertNoLocAuth;
CXAlertView *alertWaitingGpsPos;
CXAlertView *alertGPSFailWithError;

CXAlertView *alertViewNoAccountRegistered;
CXAlertView *alertViewNoServerConfigured;

UIImagePickerController *imagePicker;

UIImage *imageTaken;
BOOL isWaitingVisible = NO;

NSDate *lastValidEventDate;

UINavigationController *navController;
UIViewController *firstViewController;

NSString *longitudeToSend;
NSString *latitudeToSend;
NSString *optionToSend;


NSUserDefaults *defaults ;
NSString *serverURL ;
NSString *cellulare ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    [[_button2 layer] setBorderWidth:2.0f];
    [[_button2 layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[_button2 layer] setMasksToBounds:YES];
    [[_button2 layer] setCornerRadius:5.0];

 
    [[_buttonOpzioneAOutlet layer] setBorderWidth:2.0f];
    [[_buttonOpzioneAOutlet layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[_buttonOpzioneAOutlet layer] setMasksToBounds:YES];
    [[_buttonOpzioneAOutlet layer] setCornerRadius:5.0];
    
    [[_buttonOpzioneCOutlet layer] setBorderWidth:2.0f];
    [[_buttonOpzioneCOutlet layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[_buttonOpzioneCOutlet layer] setMasksToBounds:YES];
    [[_buttonOpzioneCOutlet layer] setCornerRadius:5.0];

    defaults = [NSUserDefaults standardUserDefaults];
    serverURL = [defaults stringForKey:@"server_url"];
    cellulare = [defaults stringForKey:@"numero_cellulare"];
    //self.view.backgroundColor = [UIColor clearColor];
    //NSLog(@"rootviewcontroller:%@", self.window.rootViewController);
}


- (IBAction)didPressSpinnerViewButton:(id)sender
{
    
    BOOL useCustomView = NO;
    
    RNBlurModalView *modal;
    if (useCustomView) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        
        
        UIColor *whiteColor = [UIColor colorWithRed:0.816 green:0.788 blue:0.788 alpha:1.000];
        
        view.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.8f];
        view.layer.borderColor = whiteColor.CGColor;
        view.layer.borderWidth = 2.f;
        view.layer.cornerRadius = 10.f;
        CGFloat defaultWidth = 280.f;
        CGRect frame = CGRectMake(0, 0, defaultWidth, 0);
        CGFloat padding = 10.f;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, defaultWidth - padding * 2.f, 0)];
        titleLabel.text = @"semplice messaggio";
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.f];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.shadowColor = [UIColor blackColor];
        titleLabel.shadowOffset = CGSizeMake(0, -1);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        //[titleLabel autoHeight];
        titleLabel.numberOfLines = 0;
        //titleLabel.top = padding;
        [view addSubview:titleLabel];
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, defaultWidth - padding * 2.f, 0)];
        messageLabel.text = @"messaggio secondario";
        messageLabel.numberOfLines = 0;
        messageLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.f];
        messageLabel.textColor = titleLabel.textColor;
        messageLabel.shadowOffset = titleLabel.shadowOffset;
        messageLabel.shadowColor = titleLabel.shadowColor;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.backgroundColor = [UIColor clearColor];
        
        view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        
        view.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
        indicator.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
        [view addSubview:indicator];
        [indicator startAnimating];
        
        
        messageLabel.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
        [view addSubview:messageLabel];
        modal = [[RNBlurModalView alloc] initWithParentView:self.view view:view];
        //NSLog(@"Entrato");
    }
    else {
        
        modal = [[RNBlurModalView alloc] initWithParentView:self.view title:@"ciao a tutti." message:@"Messaggio 1" withIndicator:false];
        
        
        modal.defaultHideBlock = ^{
            //NSLog(@"Code called after the modal view is hidden");
        };
    }
    [self.view addSubview:modal];
    [modal show];
    
}

- ( void )removeAlert
{
	[alert dismissWithClickedButtonIndex:0 animated:YES];
}




// metodo delega della chiusura della alertview
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ){
        
    }
    if( buttonIndex == 1 ){
        [self doActionAfterOkButtonPressed];
    }
    //vado nella vista principale
    /*
	UIStoryboard *mainStoryboard = self.storyboard;
	UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"segueToNewCamera"];

	[self presentViewController:vc animated:YES completion:nil];*/
}

- (IBAction)buttontest:(id)sender{
	UIStoryboard *mainStoryboard = self.storyboard;
	UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ConfirmPageSendPhoto"];
    
	[self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)seguente:(id)sender {
    [self performSegueWithIdentifier: @"segueToNewCamera" sender: self];
}


int nextLevelHasNotBeenCalled;




- (void) doActionAfterOkButtonPressed{
    //NSLog(@"Apro la fotocamera");

    
    [self openCamera];

    //NSLog(@"Fotocamera aperta\n");
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{

    [picker dismissViewControllerAnimated:YES completion:nil];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //prendo l'immagine scattata
    imageTaken = [info objectForKey:UIImagePickerControllerOriginalImage];

    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.locationManager stopUpdatingLocation];
        
    	UIStoryboard *mainStoryboard = self.storyboard;
        SA_ConfirmPageSendPhotoViewController *vc = (SA_ConfirmPageSendPhotoViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"ConfirmPageSendPhoto"];
        
        //passo tutti i dati
        [vc setImageToSend:imageTaken];
        [vc setLatitudeToSend:latitudeToSend];
        [vc setLongitudeToSend:longitudeToSend];
        [vc setOptionToSend:optionToSend];
        
        [self presentViewController:vc animated:YES completion:nil];
        
        //[self performSegueWithIdentifier:@"segueConfirmPhotoPage" sender:self];
    
    }];
    [picker dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"foto scattata!!!");

}


- (IBAction)buttonOptionsAction:(id)sender
{
    
    
    if( [cellulare isEqualToString:@""] ){
        if( !alertViewNoAccountRegistered ){
            alertViewNoAccountRegistered = [[CXAlertView alloc] initWithTitle:@"Impossibile Accedere" message:@"Nessun Account Registrato. Si prega di registrare un account." cancelButtonTitle:@"OK"];
            [self setCXAlertWithCustomColors:alertViewNoAccountRegistered];
        }
        [alertViewNoAccountRegistered show];

    }else

    if( [serverURL isEqualToString:@""] ){
        
        UIAlertView * alert2 = [[UIAlertView alloc] initWithTitle:@"ciao" message:@"messaggio" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert2 show];
    }else{
    
    //NSLog(@"%@", serverURL);
    

    optionToSend = [(UIButton*)sender currentTitle];
    
    
    lastValidEventDate = [NSDate date];
    if ( ![CLLocationManager locationServicesEnabled] ){
        
        if(!alertGPSNotActive){
            alertGPSNotActive = [[CXAlertView alloc] initWithTitle:@"GPS non attivo." message:@"Senza l’attivazione del GPS non sarà possibile completare la procedura" cancelButtonTitle:@"OK"];
            alertGPSNotActive = [self setCXAlertWithCustomColors:alertGPSNotActive];
        }
        [alertGPSNotActive show];
        
    }else{
        //controllo che il gestore gps sia attivo in caso contrario lo creo
        if( self.locationManager == nil ){
            _locationManager = [[CLLocationManager alloc] init];
            
            _locationManager.delegate = self;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            _locationManager.distanceFilter = 5; //è espresso in metri
            self.locationManager = _locationManager;
    }
        //controllo se l'utente ha autorizzato l'app per ottenere e gestire la posizione individuata dal gps
        if([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized){
            
            
            if(!alertNoLocAuth){
                alertNoLocAuth = [[CXAlertView alloc] initWithTitle:@"Localizzazione non consentita per questa app." message:@"Si prega di autorizzare Localizzazione da Privacy." cancelButtonTitle:@"OK"];
                alertNoLocAuth = [self setCXAlertWithCustomColors:alertNoLocAuth];
            }
            [alertNoLocAuth show];
            
        }else if ([CLLocationManager locationServicesEnabled]) {
            
            
            //faccio apparire l'alert con rotellina
            
            if( !alertWaitingGpsPos ){
                alertWaitingGpsPos = [[CXAlertView alloc] initWithTitle:@"Rilevazione Posizione, Attendere..." withIndicator:YES cancelButtonTitle:@"Cancella"];
                
                alertWaitingGpsPos = [self setCXAlertWithCustomColors:alertWaitingGpsPos];
            }
            [alertWaitingGpsPos show];
            
            [self.locationManager startUpdatingLocation];
            
            
            
        }

    
    }

    }
}




-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    NSDate *eventDate = newLocation.timestamp;
    NSTimeInterval howRecent;
    
    if (lastValidEventDate == nil) {
        howRecent = [eventDate timeIntervalSinceNow];
        lastValidEventDate = eventDate;
    }else{
        howRecent = [lastValidEventDate timeIntervalSinceNow];
    }
    
    //NSLog(@"Evento gps lanciato");
    
    //controllo nuovi aggiornamenti dello spostamento ogni 3 secondi
    if (  abs(howRecent) > 3.0 && (newLocation.coordinate.latitude != oldLocation.coordinate.latitude ||  newLocation.coordinate.longitude != oldLocation.coordinate.longitude) ) {
        lastValidEventDate = eventDate;
        
        if( alertWaitingGpsPos.isVisible ){
            
            [alertWaitingGpsPos dismiss];
            
            latitudeToSend = [NSString stringWithFormat:@"%+.6f",newLocation.coordinate.latitude];
            longitudeToSend = [NSString stringWithFormat:@"%+.6f",newLocation.coordinate.longitude];
            
            //NSLog(@"latitudine:%+.6f,longitudine%+.6f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
            
            //mostro l'alert per la richiesta della macchina fotografica

            
            if(!modalViewAskTakePhoto.isVisible ){
                if( !modalViewAskTakePhoto ){
                    modalViewAskTakePhoto = [[CXAlertView alloc] initWithTitle:@"attenzione" message:@"vuoi scattare una foto?" cancelButtonTitle:@"Cancella"];
                    
                    [modalViewAskTakePhoto addButtonWithTitle:@"OK"
                                                         type:CXAlertViewButtonTypeDefault
                                                      handler:^(CXAlertView *alertView, CXAlertButtonItem *button) {
                                                        [modalViewAskTakePhoto dismiss];
                                                      }];
                    modalViewAskTakePhoto = [self setCXAlertWithCustomColors:modalViewAskTakePhoto];
                }
                modalViewAskTakePhoto.didDismissHandler = ^(CXAlertView *modalViewAskTakePhoto) {
                    //parte la fotocamera
                    //[self doActionAfterOkButtonPressed];
                    [self performSelectorOnMainThread:@selector(openCamera) withObject:nil waitUntilDone:NO];
             
                    
                };
                [modalViewAskTakePhoto show];
            }

        }
        
    }
    
}

-(CXAlertView*) setCXAlertWithCustomColors:(CXAlertView*)alertView{
    if (alertView) {
        [alertView setViewBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.9f]];
        [alertView setTitleColor:[UIColor whiteColor]];
        [alertView setViewBorderColor:[UIColor whiteColor]];
        [alertView setMessageColor:[UIColor whiteColor]];
        alertView.showBlurBackground = YES;
    }

    return alertView;
}

-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    [self performSelectorOnMainThread:@selector(manageErrorGPSAlert) withObject:nil waitUntilDone:NO];
}

-( void ) manageErrorGPSAlert{
    if( alertWaitingGpsPos.isVisible || modalViewAskTakePhoto.isVisible ){
        
        if( alertWaitingGpsPos.isVisible){
            [alertWaitingGpsPos dismiss];
        }
        
        if( modalViewAskTakePhoto.isVisible){
            [modalViewAskTakePhoto dismiss];
        }
        
        
        if( !alertGPSFailWithError ){
            alertGPSFailWithError = [[CXAlertView alloc] initWithTitle:@"Errore GPS" message:@"Impossibile recuperare la posizione." cancelButtonTitle:@"Cancella"];
            
            alertGPSFailWithError = [self setCXAlertWithCustomColors:alertGPSFailWithError];
        }
        [alertGPSFailWithError show];
    }

}

- (void) openCamera{
    
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        if(!_imagePicker){
        _imagePicker = [[UIImagePickerController alloc] init];
        
        _imagePicker.delegate = self;
        _imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        _imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        _imagePicker.allowsEditing = NO;
        }
        [self presentViewController:_imagePicker animated:YES completion:nil];
        //[self performSelector:@selector(presentCameraView) withObject:nil afterDelay:0.5f];
        
    }

}

- (IBAction)Fotocamera:(id)sender {
    [self openCamera];
    
}

-(void) actionAfterDismissDialog{
}

-(void) viewDidAppear:(BOOL)animated{
    //[self listSubviewOfViews:self.view];
}

-(void) listSubviewOfViews:(UIView*)view{
    NSArray * subviews = [view subviews];
    if([subviews count] == 0) return;
    
    for (UIView* subview in subviews) {
        //NSLog(@"%@",subview);
        [self listSubviewOfViews:subview];
    }
    
}

-(void)presentCameraView{
    [self presentViewController:_imagePicker
                       animated:YES completion:nil];
}

@end