//
//  ContentViewController.h
//  PhotoSend
//
//  Created by SA_ on 11/16/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) id dataObject;
@end
