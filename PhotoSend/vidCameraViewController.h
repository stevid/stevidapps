//
//  vidCameraViewController.h
//  PhotoSend
//
//  Created by SteVid on 11/12/13.
//  Copyright (c) 2013 SteVid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface vidCameraViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *cameraView;


@end
