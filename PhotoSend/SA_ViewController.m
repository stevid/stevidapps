//
//  SA_ViewController.m
//  PhotoSend
//
//  Created by SA_ on 10/16/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_ViewController.h"

@interface SA_ViewController ()

@end

@implementation SA_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor clearColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //log
}


- (IBAction)exit:(id)sender {
    //show confirmation message to user
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Conferma"
                                                    message:@"Vuoi veramente uscire dall'applicazione?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancella"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically

        //free(1);
        int *x = NULL; *x = 42;
        //[NSObject doesNotRecognizeSelector];
        //exit app when app is in background
        //[UIApplication dealloc];
    }
}

@end
