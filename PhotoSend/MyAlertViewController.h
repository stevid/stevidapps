//
//  MyAlertViewController.h
//  PhotoSend
//
//  Created by SA_ on 11/10/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "SA_ActionFromMyAlertView.h"
#import "SA_Page2.h"

@interface MyAlertViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *vista;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
- (UIView*)generateModalView:(NSString*)title message:(NSString*)message;
- (UIView*)generateSimpleModalViewWithTitle:(NSString*)title message:(NSString*)message;
- (UIView*)generateModalViewWithTitleAndIndicator:(NSString*)title message:(NSString*)message;
- (UIView*)generateModalViewWithTitleAndIndicatorWithButton:(NSString*)title message:(NSString*)message;
- (UIView*)generateModalViewWithTitleAndButtons:(NSString*)title message:(NSString*)message;


- (void)exitButtonClick;

@property SA_Page2 *delegate;

@end
