//
//  SA_ConfirmPageSendPhotoViewController.h
//  PhotoSend
//
//  Created by SA_ on 11/8/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNBlurModalView.h"
#import <Social/Social.h>

@interface SA_ConfirmPageSendPhotoViewController : UIViewController


@property (weak,nonatomic) UIImage * imageToSend;
@property (weak,nonatomic) NSString * optionToSend;
@property (weak,nonatomic) NSString * longitudeToSend;
@property (weak,nonatomic) NSString * latitudeToSend;

@property (weak, nonatomic) IBOutlet UIView *vistaBack;

@property (weak, nonatomic) IBOutlet UITextView *textArea;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewWithNewImage;
@property (weak, nonatomic) IBOutlet UISwitch *sendAlsoToFacebook;
@property (weak, nonatomic) IBOutlet UILabel *optionToSendLabel;

- (IBAction)sendPhoto:(id)sender;

- (IBAction)actionOnImage:(id)sender;
- (IBAction)actionButtonGoToMain:(id)sender;
- (IBAction)sendToFacebookSwitchAction:(id)sender;

- (void)setImageToSend:(UIImage *)snapImage ;

@property (weak,nonatomic) SLComposeViewController *mySLComposerSheet;

@end
