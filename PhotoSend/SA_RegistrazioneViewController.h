//
//  SA_RegistrazioneViewController.h
//  PhotoSend
//
//  Created by SA_ on 11/11/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SA_ActionFromMyAlertView.h"

@interface SA_RegistrazioneViewController : UIViewController <SA_ActionFromMyAlertView,NSXMLParserDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nomeTextField;
@property (weak, nonatomic) IBOutlet UITextField *cognomeTextField;
@property (weak, nonatomic) IBOutlet UITextField *numeroCellTextField;
@property (weak, nonatomic) IBOutlet UISwitch *consensoDatiPersonaliSwitch;
- (IBAction)bottoneInvioDati:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *accauntRegistratoLabel;
- (IBAction)bottoneNuovaRegistrazione:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *bottoneNuovaRegOutlet;
@property (weak, nonatomic) IBOutlet UIButton *bottoneRegistraOutlet;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (void)manageOKRegistration;
- (void)manageKORegistration;
- (void)manageErrorRegistration;

@end
