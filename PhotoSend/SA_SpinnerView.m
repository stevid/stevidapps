//
//  SA_SpinnerView.m
//  PhotoSend
//
//  Created by SA_ on 10/22/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_SpinnerView.h"

@implementation SA_SpinnerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+(SA_SpinnerView *)loadSpinnerViewIntoView:(UIView *)superView{
    //creo una vista con la stessa dimensione di quella data
    SA_SpinnerView *spinnerView = [[SA_SpinnerView alloc] initWithFrame:superView.bounds];
    
    if(!spinnerView){ return Nil;}
    
    [spinnerView setFrame:CGRectMake(200.0,100.0, 100, 100)];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[spinnerView addBackGround]];
    
    background.alpha = 0.7;
    [spinnerView addSubview:background];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //indicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    spinnerView.center = CGPointMake(superView.bounds.size.width /2, superView.bounds.size.height/2);
    UILabel *aLabel = [UILabel alloc] ;
    
    aLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 60, 10)];
    aLabel.text = @"Ciao";
    aLabel.textColor = [UIColor whiteColor];
    indicator.center = CGPointMake(spinnerView.bounds.size.width /2, spinnerView.bounds.size.height/2);
    [spinnerView addSubview:aLabel];
    [spinnerView addSubview:indicator];
    
    [indicator startAnimating];
    
    //spinnerView.backgroundColor = [UIColor blackColor];
    //[indicator setFrame:CGRectMake(200.0, 0.0, 22.0, 22.0)];

    [superView addSubview:spinnerView];
    
    CATransition *animation = [CATransition animation];
    
    [animation setType:kCATransitionFade];
    
    [[superView layer] addAnimation:animation forKey:@"layerAnimation"];
    
    return spinnerView;

}
-(void) removeSpinner{
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [[[self superview] layer] addAnimation:animation forKey:@"layerAnimation"];
    [super removeFromSuperview];
}

- (UIImage *) addBackGround {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 1);
    size_t num_locations = 2;
    
    CGFloat locations[2];
    
    CGFloat components[8]={0.4,0.4,0.4, 0.8, 0.1,0.1,0.1, 0.5};
    
    CGColorSpaceRef myColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef myGradient = CGGradientCreateWithColorComponents(myColorSpace, components, locations, num_locations);
    
    float myRadius = (self.bounds.size.width*.8)/2;
    
    CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), myGradient, self.center, 0, self.center, myRadius, kCGGradientDrawsAfterEndLocation);
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    
    CGColorSpaceRelease(myColorSpace);
    CGGradientRelease(myGradient);
    UIGraphicsEndImageContext();
    return image;

}

@end
