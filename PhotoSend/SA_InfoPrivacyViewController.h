//
//  SA_InfoPrivacyViewController.h
//  PhotoSend
//
//  Created by SA_ on 11/15/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SA_InfoPrivacyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *textView;

@end
