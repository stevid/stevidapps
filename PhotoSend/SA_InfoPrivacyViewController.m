//
//  SA_InfoPrivacyViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/15/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_InfoPrivacyViewController.h"

@interface SA_InfoPrivacyViewController ()

@end

@implementation SA_InfoPrivacyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //_textView.editable = NO;
    [[_textView layer] setBorderWidth:2.0f];
    [[_textView layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_textView layer] setMasksToBounds:YES];
    [[_textView layer] setCornerRadius:10.f];
    
    //_textView.text = @"Gentile Cliente,ai sensi dell’art. 13 del D.Lgs. 30 giugno 2003, n. 196 (Codice in materia di protezione dei dati personali), La informiamo che i dati personali da Lei forniti o comunque acquisiti all’atto della sottoscrizione della Carta Fedeltà saranno trattati nel rispetto della normativa vigente e dei principi di correttezza, liceità, trasparenza e tutela della riservatezza da essa previsti.\n";
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"normaprivacy" ofType:@"txt"];
    NSString *myText = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    //_textView.text = myText;
    [_textView loadHTMLString:myText baseURL:nil];
    //_textView.textColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
