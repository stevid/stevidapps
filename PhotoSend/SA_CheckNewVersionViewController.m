//
//  SA_CheckNewVersionViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/16/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_CheckNewVersionViewController.h"
#import "CXAlertView.h"

@interface SA_CheckNewVersionViewController ()

@end

@implementation SA_CheckNewVersionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonNewReleaseCheckActrion:(id)sender {
    
    CXAlertView *alert = [[CXAlertView alloc] initWithTitle:@"Attenzione" message:@"Nessuna nuova Versione presente nel Server. Riprovare più avanti" cancelButtonTitle:@"OK"];
    [alert setCXAlertWithCustomColors:alert];
    [alert show];
    
}
@end
