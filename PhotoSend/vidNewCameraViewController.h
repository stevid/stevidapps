//
//  vidNewCameraViewController.h
//  PhotoSend
//
//  Created by SteVid on 11/12/13.
//  Copyright (c) 2013 SteVid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface vidNewCameraViewController : UIViewController{
    
    BOOL FrontCamera;
    BOOL haveImage;
}

@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;

@property (weak, nonatomic) IBOutlet UIImageView *captureImage;
- (IBAction)snapImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *imagePreview;


@end
