//
//  MyAlertViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/10/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "MyAlertViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>

@interface MyAlertViewController ()

@end

@interface UILabel (AutoSize)
- (void)autoHeight;
@end

@interface UIView (Sizes)
@property (nonatomic) CGFloat left;
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;
@end


@interface UIView (Screenshot)
- (UIImage*)screenshot;
@end

@interface UIImage (Blur)
-(UIImage *)boxblurImageWithBlur:(CGFloat)blur;
@end

@implementation MyAlertViewController

UIView *totalView;
UIButton *okButton3;
UILabel *titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[_vista layer] setBorderWidth:2.0f];
    [[_vista layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_vista layer] setMasksToBounds:YES];
    [[_vista layer] setCornerRadius:10.f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)okButtonClick{
    NSLog(@"Bottone OK Premuto!");
    [_delegate performSegueWithIdentifier: @"segueToNewCamera" sender: self];
    /*
    if( _delegate != Nil && [_delegate conformsToProtocol:@protocol(vidActionFromMyAlertView)]){
        
        [self.delegate doActionAfterOkButtonPressed];
    }*/
    //vado nella vista principale

}

- (void)cancelButtonClick{
    
    [totalView removeFromSuperview];
    totalView = nil;
    
}

- (UIView*)generateModalViewWithTitleAndIndicatorWithButton:(NSString*)title message:(NSString*)message {
    UIView *view = [self generateSimpleModalViewWithTitle: title message: message];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    view.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
    indicator.center = CGPointMake( view.bounds.size.width /2 , view.bounds.size.height/2 + 30);
    [indicator startAnimating];
    [view addSubview:indicator];
    
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    
    CGRect frame1 = CGRectMake(0.0,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
    totalView = [[UIView alloc] initWithFrame:frame1 ];
    totalView.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
    view.center = CGPointMake(totalView.bounds.size.width /2, totalView.bounds.size.height/2);
    [totalView addSubview:view];
    
    return totalView;
}

- (UIView*)generateModalViewWithTitleAndIndicator:(NSString*)title message:(NSString*)message {
    UIView *view = [self generateModalView: title message: message];
    
     UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
     
     
     view.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
     indicator.center = CGPointMake( view.bounds.size.width /2 , view.bounds.size.height/2 + 30);
     [indicator startAnimating];
     [view addSubview:indicator];
   
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    
    CGRect frame1 = CGRectMake(0.0,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
    totalView = [[UIView alloc] initWithFrame:frame1 ];
    totalView.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
    view.center = CGPointMake(totalView.bounds.size.width /2, totalView.bounds.size.height/2);
    [totalView addSubview:view];
    
    return totalView;
}
- (UIView*)generateModalViewWithTitleAndButtons:(NSString*)title message:(NSString*)message{
    UIView *view = [self generateModalView:title message:message];
    UIColor *whiteColor = [UIColor colorWithRed:0.816 green:0.788 blue:0.788 alpha:1.000];
    
    //aggiungo bottone ok
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    okButton.userInteractionEnabled = YES;
    [okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    okButton.frame = CGRectMake(10, 10, 80, 30);
    okButton.top = titleLabel.bottom ;
    [[okButton layer] setBorderWidth:2.0f];
    
    okButton.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9f];
    okButton.layer.borderColor = whiteColor.CGColor;
    [[okButton layer] setMasksToBounds:YES];
    [[okButton layer] setCornerRadius:5.0];
    
    okButton.center = CGPointMake( (view.bounds.size.width /2) +60 , view.bounds.size.height - 30);
    
    [okButton addTarget:self action:@selector(okButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:okButton];
    //aggiungo bottone cancel
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.userInteractionEnabled = YES;
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton setTitle:@"Cancella" forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 10, 80, 30);
    cancelButton.top = titleLabel.bottom ;
    [[cancelButton layer] setBorderWidth:2.0f];
    
    cancelButton.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9f];
    cancelButton.layer.borderColor = whiteColor.CGColor;
    [[cancelButton layer] setMasksToBounds:YES];
    [[cancelButton layer] setCornerRadius:5.0];
    
    cancelButton.center = CGPointMake( (view.bounds.size.width /2) -60 , view.bounds.size.height - 30);
    
    [cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:cancelButton]; //
    
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    
    CGRect frame1 = CGRectMake(0.0,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
    totalView = [[UIView alloc] initWithFrame:frame1 ];
    totalView.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
    view.center = CGPointMake(totalView.bounds.size.width /2, totalView.bounds.size.height/2);
    [totalView addSubview:view];
    
    return totalView;
}

- (UIView*)generateSimpleModalViewWithTitle:(NSString*)title message:(NSString*)message{
    
    UIView *view = [self generateModalView:title message:message];
    UIColor *whiteColor = [UIColor colorWithRed:0.816 green:0.788 blue:0.788 alpha:1.000];
    
    //aggiungo il bottone
    okButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
    okButton3.userInteractionEnabled = YES;
    //okButton3 = [UIButton  buttonWithType:UIButtonTypeRoundedRect];

    [okButton3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okButton3 setTitle:@"Chiudi" forState:UIControlStateNormal];
    okButton3.frame = CGRectMake(10, 10, 80, 30);
    okButton3.top = titleLabel.bottom ;
    [[okButton3 layer] setBorderWidth:2.0f];
    
    okButton3.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9f];
    okButton3.layer.borderColor = whiteColor.CGColor;

    
    [[okButton3 layer] setMasksToBounds:YES];
    [[okButton3 layer] setCornerRadius:5.0];
    
    okButton3.center = CGPointMake( view.bounds.size.width /2 , view.bounds.size.height - 30);    
    
    [okButton3 addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];


    
    [view addSubview:okButton3];
    
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    
    CGRect frame1 = CGRectMake(0.0,0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    
    totalView = [[UIView alloc] initWithFrame:frame1 ];
    totalView.center = CGPointMake(view.bounds.size.width /2, view.bounds.size.height/2);
    view.center = CGPointMake(totalView.bounds.size.width /2, totalView.bounds.size.height/2);
    [totalView addSubview:view];
    
    return totalView;
}

- (UIView*)generateModalView:(NSString*)title message:(NSString*)message{
    CGFloat defaultWidth = 280.f;
    CGRect frame = CGRectMake(0, 0, defaultWidth, 0);
    CGFloat padding = 20.f;
    UIView *view = [[UIView alloc] initWithFrame:frame];
    
    UIColor *whiteColor = [UIColor colorWithRed:0.816 green:0.788 blue:0.788 alpha:1.000];
    
    view.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9f];
    view.layer.borderColor = whiteColor.CGColor;
    view.layer.borderWidth = 2.f;
    view.layer.cornerRadius = 10.f;
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, defaultWidth - padding * 2.f, 0)];
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor blackColor];
    titleLabel.shadowOffset = CGSizeMake(0, -1);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    [titleLabel autoHeight];
    titleLabel.numberOfLines = 0;
    titleLabel.top = padding;
    [view addSubview:titleLabel];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, defaultWidth - padding * 2.f, 0)];
    messageLabel.text = message;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.f];
    messageLabel.textColor = titleLabel.textColor;
    messageLabel.shadowOffset = titleLabel.shadowOffset;
    messageLabel.shadowColor = titleLabel.shadowColor;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.backgroundColor = [UIColor clearColor];
    [messageLabel autoHeight];
    messageLabel.top = titleLabel.bottom + padding;
    [view addSubview:messageLabel];
    
    view.height = messageLabel.bottom + padding + padding + padding + padding;
    return view;
}

@end

@implementation UILabel (AutoSize)

- (void)autoHeight {
    CGRect frame = self.frame;
    CGSize maxSize = CGSizeMake(frame.size.width, 9999);
    CGSize expectedSize = [self.text sizeWithFont:self.font constrainedToSize:maxSize lineBreakMode:self.lineBreakMode];
    frame.size.height = expectedSize.height;
    [self setFrame:frame];
}

@end
@implementation UIView (Sizes)

- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

@end
@implementation UIView (Screenshot)

- (UIImage*)screenshot{
    /*
    UIGraphicsBeginImageContext(parentView.bounds.size);
    if( [parentView respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)] ){
        [parentView drawViewHierarchyInRect:parentView.bounds afterScreenUpdates:YES];
    }else{
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    }*/
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // hack, helps w/ our colors when blurring
    NSData *imageData = UIImageJPEGRepresentation(image, 1); // convert to jpeg
    image = [UIImage imageWithData:imageData];
    image = [image boxblurImageWithBlur:0.2f];
  
    return image;
}

@end

@implementation UIImage (Blur)

-(UIImage *)boxblurImageWithBlur:(CGFloat)blur {
    if (blur < 0.f || blur > 1.f) {
        blur = 0.5f;
    }
    int boxSize = (int)(blur * 40);
    boxSize = boxSize - (boxSize % 2) + 1;
    
    CGImageRef img = self.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    
    vImage_Error error;
    
    void *pixelBuffer;
    
    
    //create vImage_Buffer with data from CGImageRef
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    //create vImage_Buffer for output
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
    
    if(pixelBuffer == NULL)
        NSLog(@"No pixelbuffer");
    
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    // Create a third buffer for intermediate processing
    void *pixelBuffer2 = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
    vImage_Buffer outBuffer2;
    outBuffer2.data = pixelBuffer2;
    outBuffer2.width = CGImageGetWidth(img);
    outBuffer2.height = CGImageGetHeight(img);
    outBuffer2.rowBytes = CGImageGetBytesPerRow(img);
    
    //perform convolution
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer2, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    error = vImageBoxConvolve_ARGB8888(&outBuffer2, &inBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
    
    if (error) {
        NSLog(@"error from convolution %ld", error);
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             kCGImageAlphaNoneSkipLast);
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    CGColorSpaceRelease(colorSpace);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    
    CGImageRelease(imageRef);
    
    return returnImage;
}
@end