//
//  CustomAlertView.h
//  PhotoSend
//
//  Created by SA_ on 11/13/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIAlertView <UIApplicationDelegate, UIAlertViewDelegate>

@end
