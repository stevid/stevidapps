//
//  SA_ActionFromMyAlertView.h
//  PhotoSend
//
//  Created by SA_ on 11/11/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SA_ActionFromMyAlertView <NSObject>
@optional
-(void) doActionAfterOkButtonPressed;
-(void) actionAfterExitButtonPressed;
@end
