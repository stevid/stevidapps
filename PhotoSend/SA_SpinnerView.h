//
//  SA_SpinnerView.h
//  PhotoSend
//
//  Created by SA_ on 10/22/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SA_SpinnerView : UIView

+(SA_SpinnerView *)loadSpinnerViewIntoView:(UIView *)superView;
-(void) removeSpinner;

@end
