//
//  SA_ConfirmPageSendPhotoViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/8/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_ConfirmPageSendPhotoViewController.h"
#import "Reachability.h"
#import "CXAlertView.h"
#import "Base64.h"


@implementation UIImage (Extended)

- (NSString *)base64String {
    NSData * data = [UIImagePNGRepresentation(self) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return [NSString stringWithUTF8String:[data bytes]];
}

@end

@interface SA_ConfirmPageSendPhotoViewController ()

@end

@implementation SA_ConfirmPageSendPhotoViewController

BOOL isInternetActive = false;
BOOL isHostActive     = false;
BOOL isAlertDisplayed = false;

NSString *encriptedStringImageToSend;
NSString *encriptedStringOptionToSend;
NSString *encriptedStringLongitudeToSend;
NSString *encriptedStringLatitudeToSend;
NSString *encriptedStringCellulare;
NSString *encriptedStringPassword;
NSString *encriptedStringMessageToSend;
NSString *serverURL;
NSString *cellulare;
NSString *password;

CXAlertView *alertViewMessage;
CXAlertView *alertViewHostMessage;
UIAlertView *alertViewWaitRegistration;
CXAlertView *alert;

NSString *currentElement;
NSString *registrationResultCode;
NSString *registrationResultCodeDescription;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor clearColor];
    [[_vistaBack layer] setBorderWidth:2.0f];
    [[_vistaBack layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_vistaBack layer] setMasksToBounds:YES];
    [[_vistaBack layer] setCornerRadius:10.f];
    
    [[_textArea layer] setBorderWidth:2.0f];
    [[_textArea layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_textArea layer] setMasksToBounds:YES];
    [[_textArea layer] setCornerRadius:10.f];
    
    [_imageViewWithNewImage setImage:[self imageWithImage:_imageToSend scaledToSize:CGSizeMake(100.0f, 120.0f)]];
    /*
    NSData *data = UIImageJPEGRepresentation([self imageWithImage:_imageToSend scaledToSize:CGSizeMake(200.0f, 300.0f)], 1);
    UIImage *myImage=[UIImage imageWithData:data];
    [_imageViewWithNewImage setImage:myImage];*/
    
    [[_imageViewWithNewImage layer] setBorderWidth:2.0f];
    [[_imageViewWithNewImage layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[_imageViewWithNewImage layer] setMasksToBounds:YES];
    [[_imageViewWithNewImage layer] setCornerRadius:5.0];
    _optionToSendLabel.text = _optionToSend;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



- (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


- (IBAction)sendPhoto:(id)sender {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        serverURL = [defaults stringForKey:@"server_url"];
        cellulare = [defaults stringForKey:@"numero_cellulare"];
        password  = [defaults stringForKey:@"password"];
    
        //NSLog(@"PASSWORD RILEVATA:%@", password);
        //NSLog(@"%@", serverURL);
    
    NSData *imageData = UIImageJPEGRepresentation([self imageWithImage:_imageToSend scaledToSize:CGSizeMake(200.0f, 300.0f)], 1);
    [Base64 initialize];
    

    encriptedStringImageToSend = [imageData base64EncodedStringWithOptions:0];

    //encriptedStringImageToSend = [Base64 encode:imageData];
    
    
    NSData *data = [_optionToSend dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringOptionToSend = [Base64 encode:data];
    
    encriptedStringOptionToSend = [self base64String:_optionToSend];
    
    
    data = [_latitudeToSend dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringLatitudeToSend = [Base64 encode:data ];
    
    data = [_longitudeToSend dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringLongitudeToSend = [Base64 encode:data ];
    
    data = [cellulare dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringCellulare = [Base64 encode:data ];
    
    data = [password dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringPassword = [Base64 encode:data ];
    
    data = [[_textArea.text stringByReplacingOccurrencesOfString:@"'" withString:@"''"] dataUsingEncoding:NSASCIIStringEncoding];
    [Base64 initialize];
    encriptedStringMessageToSend = [Base64 encode:data ];
    
    //controllo la connessione
    [self checkNetworkStatus:serverURL];
    
    if ( isInternetActive == NO ) {
        
        alert = [[CXAlertView alloc] initWithTitle:@"Internet non presente" message:@"Attivare la connessione per effettuare la registrazione" cancelButtonTitle:@"OK"];
        [alert setCXAlertWithCustomColors:alert];
        [alert show];
        return;
        
    }else if ( isHostActive == NO ) {
        
        alert = [[CXAlertView alloc] initWithTitle:@"Server non raggiungibile" message:@"Controllare dalle impostazioni l'url" cancelButtonTitle:@"OK"];
        [alert setCXAlertWithCustomColors:alert];
        [alert show];
        return;
        
    }
    
    [self checkInsertText];
    
}

- (void)checkInsertText{
    
    if( [_textArea.text isEqualToString:@""] ){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Conferma" message:@"Vuoi inviare i dati senza aggiungere un messaggio?" delegate:self cancelButtonTitle:@"Cancella" otherButtonTitles:@"Invia Pure", nil];
        alert.tag = 01;
        [alert show];
        
    }else{
        
            [self sendRegistrationData];
        
    }
}

-(void) sendFacebookPost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        _mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        _mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [_mySLComposerSheet setInitialText:[NSString stringWithFormat:_textArea.text,_mySLComposerSheet.serviceType]]; //the message you want to post
        [_mySLComposerSheet addImage:_imageToSend]; //an image you could post
        //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
        [self presentViewController:_mySLComposerSheet animated:YES completion:nil];
    }else{

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Accont facebook non collegato o non abilitato, andare in Impostazioni->Facebook e registrare un Account, a quel punto sarà possibile inviare foto e messaggio." delegate:self
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.tag = 200;
        [alert show];
    
    }
    [_mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Azione Cancellata";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post inserito.";
                break;
            default:
                break;
        } //check if everythink worked properly. Give out a message on the state.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:self
 cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.tag = 100;
        [alert show];
    }];


}

- (void) sendRegistrationData{
    
    alertViewWaitRegistration = [[UIAlertView alloc] initWithTitle:@"Attendere prego" message:@"Connessione in corso..." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alertViewWaitRegistration show];
    
    NSString *connectionString = [[NSString alloc] initWithFormat:@"%@/photosend/registra_photo.php",serverURL];
    
    NSURL *URL = [NSURL URLWithString:connectionString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    //NSLog([NSString stringWithFormat:@"DATI INVATI VIA POST : cellulare=%@&password=%@&longitudine=%@&latitudine=%@&immagine=%@&opzione=%@&messaggio=%@",encriptedStringCellulare,encriptedStringPassword,encriptedStringLongitudeToSend,encriptedStringLatitudeToSend,encriptedStringImageToSend,encriptedStringOptionToSend,encriptedStringMessageToSend]);
    NSString *params = [NSString stringWithFormat:@"cellulare=%@&password=%@&longitudine=%@&latitudine=%@&immagine=%@&opzione=%@&messaggio=%@",encriptedStringCellulare,encriptedStringPassword,encriptedStringLongitudeToSend,encriptedStringLatitudeToSend,encriptedStringImageToSend,encriptedStringOptionToSend,encriptedStringMessageToSend];
    //NSString *params = @"";
    NSData *data = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:data];
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[[NSOperationQueue alloc] init]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             //NSLog(@"connessione OK");
             //NSLog([NSString stringWithFormat:@"%@",data]);
             //NSLog([NSString stringWithFormat:@"status code: %i",[httpResponse statusCode]]);
             // DO YOUR WORK HERE
             NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             //NSLog(@"%@",strData);
             NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
             [parser setDelegate:self];
             [parser parse];
             
           
         }else{
             //NSLog(@"connessione KO");
             
             [self performSelectorOnMainThread:@selector(manageConnectionKO) withObject:nil waitUntilDone:NO];
         }
         
     }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NSLog(@"RILEVATO Alert con Tag:%i",alertView.tag);
    if ( ( alertView.tag == 100 || alertView.tag == 200) ) {
        [self sendRegistrationData];
        
    }else if( buttonIndex == 1 && alertView.tag == 01 ){
        [self sendRegistrationData];
    }else if( alertView.tag == 003 ){
        if( _sendAlsoToFacebook.isOn ){
            [self sendFacebookPost];
        }else{
            [self goToMainView];
        }
    }
    else{
        [self goToMainView];
    }
}

- (void) goToMainView{
    /*
	UIStoryboard *mainStoryboard = self.storyboard;
	UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Pagina 2"];

	[self presentViewController:vc animated:YES completion:nil];*/
    
    [self performSegueWithIdentifier:@"segueOptionsView" sender:self];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    if ([_textArea isFirstResponder] && [touch view] != _textArea) {
        
        [_textArea resignFirstResponder];
    }
    // My comment : If you have several text controls copy/paste/modify a block above and you are DONE!
}

- (void) checkNetworkStatus:(NSString*) hostName{
    //connessione
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    
    //host
    //Reachability *reach = [Reachability reachabilityWithHostName: @"www.apple.com"];
    
    NSString *string;
    switch(status) {
        case NotReachable:
            string = @"Not Reachable";
            isInternetActive = NO;
            break;
        case ReachableViaWiFi:
            string = @"Reachable via WiFi";
            isInternetActive = YES;
            break;
        case ReachableViaWWAN:
            string = @"Reachable via WWAN";
            isInternetActive = YES;
            break;
        default:
            string = @"Unknown";
            isInternetActive = NO;
            break;
    }
    
    reach = [Reachability reachabilityWithHostName: hostName];
    switch(status) {
        case NotReachable:
            string = @"Not Reachable";
            isHostActive = NO;
            break;
        case ReachableViaWiFi:
            string = @"Reachable via WiFi";
            isHostActive = YES;
            break;
        case ReachableViaWWAN:
            string = @"Reachable via WWAN";
            isHostActive = YES;
            break;
        default:
            string = @"Unknown";
            isHostActive = NO;
            break;
    }
    
}
-(void) actionAfterDismissDialog{
    isAlertDisplayed = false;
    [self goToMainView];
}

- (IBAction)actionOnImage:(id)sender {
    
    [_imageViewWithNewImage setImage:[self imageWithImage:_imageToSend scaledToSize:CGSizeMake(100.0f, 120.0f)]];
    
}

- (void)setImageToSend:(UIImage *)snapImage {
    _imageToSend = snapImage;

     
}

- (IBAction)actionButtonGoToMain:(id)sender {
}

- (IBAction)sendToFacebookSwitchAction:(id)sender {
    if(![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        CXAlertView *alertFB = [[CXAlertView alloc] initWithTitle:@"Facebook non disponibile" message:@"Accont facebook non collegato o non abilitato, andare in Impostazioni->Facebook e registrare un Account, a quel punto sarà possibile inviare foto e messaggio." cancelButtonTitle:@"OK"];
        [alertFB setCXAlertWithCustomColors:alertFB];
        [alertFB show];
        [_sendAlsoToFacebook setOn:NO animated:YES];
    }
    
}

-(void) setOptionToSend:(NSString*)optionToSend{
    _optionToSend = optionToSend;
}

-(void) setLongitudeToSend:(NSString *)longitudeToSend{
    _longitudeToSend = longitudeToSend;
}

-(void) setLatitudeToSend:(NSString *)latitudeToSend{
    _latitudeToSend = latitudeToSend;
}


-(UIImage*) imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize{

    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}
- (void) parserDidStartDocument:(NSXMLParser *)parser{
    //NSLog(@"Parsing documento partito.");
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //NSLog([NSString stringWithFormat:@"START elemento trovato:%@",elementName]);
    if( [elementName isEqualToString:@"code"] ){
        currentElement = @"code";
    }
    if( [elementName isEqualToString:@"description"] ){
        currentElement = @"description";
    }
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //NSLog([NSString stringWithFormat:@"valore elemento trovato:%@",string]);
    if( [currentElement isEqualToString:@"code"] ){
        registrationResultCode = string;
    }
    if( [currentElement isEqualToString:@"description"] ){
        registrationResultCodeDescription = string;
    }
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //NSLog([NSString stringWithFormat:@"END elemento:%@",elementName]);
    if( [elementName isEqualToString:@"code"] ){
        currentElement = @"";
    }
    if( [elementName isEqualToString:@"description"] ){
        currentElement = @"";
    }
}

-(void) parserDidEndDocument:(NSXMLParser *)parser{
    //NSLog(@"FINE DOCUMENTO");
    
    if( [registrationResultCode isEqualToString:@"OK"] ){
        [self performSelectorOnMainThread:@selector(manageOKRegistration) withObject:nil waitUntilDone:NO];
    }
    
    if( [registrationResultCode isEqualToString:@"KO"] ){
        [self performSelectorOnMainThread:@selector(manageKORegistration) withObject:nil waitUntilDone:NO];
    }
    //NSLog(@"FINE DOCUMENTO END");
}

-(void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"Errore: %i", [parseError code]);
    [self performSelectorOnMainThread:@selector(manageErrorRegistration) withObject:nil waitUntilDone:NO];
}

- (void)manageOKRegistration{
    //NSLog(@"Managing OK");
    [alertViewWaitRegistration dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulazioni!" message:@"Registrazione sul Server effettuata con successo" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alert.tag = 003;
    [alert show];
    //NSLog(@"alert OK creato");
}

- (void)manageConnectionKO{
    [alertViewWaitRegistration dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Impossibile contattare il server" message:@"Impossibile completare l'operazione" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)manageKORegistration{
    [alertViewWaitRegistration dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registrazione non avvenuta" message:registrationResultCodeDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)manageErrorRegistration{
    [alertViewWaitRegistration dismissWithClickedButtonIndex:0 animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errore Interno in fase di registrazione" message:@"Impossibile completare la Registrazione" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
@end
