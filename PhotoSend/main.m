//
//  main.m
//  PhotoSend
//
//  Created by SA_ on 10/16/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SA_AppDelegate.h"
int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SA_AppDelegate class]));
    }
}
