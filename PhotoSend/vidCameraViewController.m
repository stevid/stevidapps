//
//  vidCameraViewController.m
//  PhotoSend
//
//  Created by SteVid on 11/12/13.
//  Copyright (c) 2013 SteVid. All rights reserved.
//

#import "vidCameraViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>

@interface vidCameraViewController ()

@end

@implementation vidCameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        //kUTTypeImage
        
        //[[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:self animated:YES completion:nil];
        [[[[UIApplication sharedApplication] delegate] window] setRootViewController:self];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        // Do any additional setup after loading the view.
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self goToPrevView];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [picker.view removeFromSuperview];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    //prendo l'immagine scattata
    UIImage *imageTaken = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    	UIStoryboard *mainStoryboard = self.storyboard;
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ConfirmPageSendPhoto"];
        
        [self presentViewController:vc animated:YES completion:nil];
        
        [picker.view removeFromSuperview];
        [picker.view.superview removeFromSuperview];
        
    }];
    //[_imagePicker.view removeFromSuperview];
    [self goToPrevView];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"foto scattata dalla Vista!!!");
    
}
- (void) goToPrevView{
    /*
	UIStoryboard *mainStoryboard = self.storyboard;
	UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"mainView"];
    
	[self presentViewController:vc animated:YES completion:nil];*/
    [self performSegueWithIdentifier:@"seguePagina2" sender:self];
    
}



@end
