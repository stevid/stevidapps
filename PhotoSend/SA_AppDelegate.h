//
//  SA_AppDelegate.h
//  PhotoSend
//
//  Created by SA_ on 10/24/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SA_AppDelegate : UIResponder

@property (strong,nonatomic) UIWindow *window;

@end
