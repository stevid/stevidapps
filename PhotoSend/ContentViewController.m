//
//  ContentViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/16/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "ContentViewController.h"

@interface ContentViewController ()

@end

@implementation ContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[_webView layer] setBorderWidth:2.0f];
    [[_webView layer] setBorderColor:[UIColor grayColor].CGColor];
    [[_webView layer] setMasksToBounds:YES];
    [[_webView layer] setCornerRadius:10.f];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_webView loadHTMLString:_dataObject
                     baseURL:[NSURL URLWithString:@""]];
}
@end
