//
//  SA_RegistrazioneViewController.m
//  PhotoSend
//
//  Created by SA_ on 11/11/13.
//  Copyright (c) 2013 SA_. All rights reserved.
//

#import "SA_RegistrazioneViewController.h"
#import <Foundation/Foundation.h>
#import "MyAlertViewController.h"
#import "Reachability.h"
#import "CXAlertView.h"
#import "SA_Page2.h"

@interface SA_RegistrazioneViewController ()

@end

@implementation SA_RegistrazioneViewController


NSString *nomeAccount;
NSString *cognomeAccount;
NSString *numeroCellulareAccount;
NSString *urlServer;
NSString *password;
NSString *serverURL;
NSString *registrationResult;

CXAlertView *alert;
UIAlertView *newAlert;

UIView *myAlert;

CXAlertView *alertViewWaitRegistration;
//MyAlertViewController *alertWaitRegistration;
//UIView *myAlertWaitRegistration;

//variabili controllo network
BOOL isInternetActive_ = false;
BOOL isHostActive_     = false;
BOOL isAlertDisplayed_ = false;

NSString *currentElement;
NSString *registrationResultCode;
NSString *registrationResultCodeDescription;

- (void) getSettingsData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    urlServer               = [defaults stringForKey:@"server_url"];
    nomeAccount             = [defaults stringForKey:@"nome"];
    cognomeAccount          = [defaults stringForKey:@"cognome"];
    numeroCellulareAccount  = [defaults stringForKey:@"numero_cellulare"];
    password                = [defaults stringForKey:@"password"];
    //NSLog(@"PASSWORD RILEVATA: %@",password);
}

- (void) setSettingsData{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nomeAccount forKeyPath:@"nome"];
    [defaults setValue:cognomeAccount forKeyPath:@"cognome"];
    [defaults setValue:numeroCellulareAccount forKeyPath:@"numero_cellulare"];
    [defaults setValue:password forKeyPath:@"password"];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_consensoDatiPersonaliSwitch setOn:NO animated:YES];
    [self getSettingsData];
    _nomeTextField.text         = nomeAccount;
    _cognomeTextField.text      = cognomeAccount;
    _numeroCellTextField.text   = numeroCellulareAccount;
    _passwordTextField.text     = password;
    
    if( ![nomeAccount isEqualToString:@""] ){
        [self disattivaCampi];
        _bottoneRegistraOutlet.hidden = YES;
        [_consensoDatiPersonaliSwitch setOn:YES animated:YES];
        _bottoneNuovaRegOutlet.hidden  = NO;
    }else{
        [self attivaCampi];
        _accauntRegistratoLabel.hidden = YES;
        _bottoneNuovaRegOutlet.hidden  = YES;
        _bottoneRegistraOutlet.hidden = NO;
    }  
}

- (void) azzeraCampi{
    [_nomeTextField setText:@""];
    [_cognomeTextField setText:@""];
    [_numeroCellTextField setText:@""];
    [_passwordTextField setText:@""];
    [_consensoDatiPersonaliSwitch setOn:NO animated:YES];
}

- (void) attivaCampi{
    [_nomeTextField setEnabled:YES];
    [_cognomeTextField setEnabled:YES];
    [_numeroCellTextField setEnabled:YES];
    [_passwordTextField setEnabled:YES];
    [_consensoDatiPersonaliSwitch setEnabled:YES];
}

-(void) disattivaCampi{
    [_nomeTextField setEnabled:NO];
    [_cognomeTextField setEnabled:NO];
    [_numeroCellTextField setEnabled:NO];
    [_passwordTextField setEnabled:NO];
    [_consensoDatiPersonaliSwitch setEnabled:NO];
}

- (void) attivaBottoniELabel{
    _accauntRegistratoLabel.hidden = YES;
    _bottoneNuovaRegOutlet.hidden  = YES;
    [self attivaCampi];
    _bottoneRegistraOutlet.hidden = NO;
}

- (void) disattivaBottoniELabel{
    [self disattivaCampi];
    _accauntRegistratoLabel.hidden = NO;
    _bottoneNuovaRegOutlet.hidden  = NO;
    _bottoneRegistraOutlet.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) doActionAfterOkButtonPressed{
    //NSLog(@"Eseguito con il Delegato");
    [myAlert removeFromSuperview];
    alert = nil;
    myAlert = nil;
    [self attivaBottoniELabel];
    [self azzeraCampi];
}

- (IBAction)bottoneInvioDati:(id)sender {
    
    //controllo i campi vuoti
    if( [[_nomeTextField text] isEqualToString:@""] || [[_cognomeTextField text] isEqualToString:@""] || [[_numeroCellTextField text] isEqualToString:@""] || !_consensoDatiPersonaliSwitch.isOn  || [[_passwordTextField text] isEqualToString:@""]){
        
        alert = [[CXAlertView alloc] initWithTitle:@"Attenzione" message:@"Mancano dati o consenso non selezionato" cancelButtonTitle:@"OK"];
        [alert setCXAlertWithCustomColors:alert];
        [alert show];
        return;
    }
    
    //recupero il server dalle impostazioni
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverURL = [defaults stringForKey:@"server_url"];
    //NSLog(@"server configurato: %@", serverURL);
    
    //controllo la connessione
    [self checkNetworkStatus:serverURL];
    
    if ( isInternetActive_ == NO ) {
        
        alert = [[CXAlertView alloc] initWithTitle:@"Internet non presente" message:@"Attivare la connessione per effettuare la registrazione" cancelButtonTitle:@"OK"];
        [alert setCXAlertWithCustomColors:alert];
        [alert show];
        return;
        
    }else if ( isHostActive_ == NO ) {
        
        alert = [[CXAlertView alloc] initWithTitle:@"Server non raggiungibile" message:@"Controllare dalle impostazioni l'url" cancelButtonTitle:@"OK"];
        [alert setCXAlertWithCustomColors:alert];
        [alert show];
        return;
        
    }
    
    newAlert = [[UIAlertView alloc] initWithTitle:@"attendere prego" message:@"connessione in corso" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [newAlert setAlpha:0.8];
    [newAlert setBackgroundColor:[UIColor blackColor]];

    [[newAlert layer] setBorderWidth:2.0f];
    [[newAlert layer] setBorderColor:[UIColor redColor].CGColor];
    [[newAlert layer] setMasksToBounds:YES];
    [[newAlert layer] setCornerRadius:5.0];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    [newAlert addSubview:indicator];
    [newAlert show];
    
    [self sendRegistrationData];
    //[NSThread detachNewThreadSelector:@selector(sendRegistrationData) toTarget:self withObject:nil];
    
}

-(void)checkRegistrationResult{

    if ([registrationResult isEqualToString:@"OK"]) {
        [self manageOKRegistration];
    }

}

- (void) parserDidStartDocument:(NSXMLParser *)parser{
    //NSLog(@"Parsing documento partito.");
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    //NSLog([NSString stringWithFormat:@"START elemento trovato:%@",elementName]);
    if( [elementName isEqualToString:@"code"] ){
        currentElement = @"code";
    }
    if( [elementName isEqualToString:@"description"] ){
        currentElement = @"description";
    }
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //NSLog([NSString stringWithFormat:@"valore elemento trovato:%@",string]);
    if( [currentElement isEqualToString:@"code"] ){
        registrationResultCode = string;
    }
    if( [currentElement isEqualToString:@"description"] ){
        registrationResultCodeDescription = string;
    }
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    //NSLog([NSString stringWithFormat:@"END elemento:%@",elementName]);
    if( [elementName isEqualToString:@"code"] ){
        currentElement = @"";
    }
    if( [elementName isEqualToString:@"description"] ){
        currentElement = @"";
    }
}

-(void) parserDidEndDocument:(NSXMLParser *)parser{
    //NSLog(@"FINE DOCUMENTO");
    
    //[alertViewWaitRegistration dismiss];
    
    if( [registrationResultCode isEqualToString:@"OK"] ){
        [self performSelectorOnMainThread:@selector(manageOKRegistration) withObject:nil waitUntilDone:NO];
        /*
        [self performSelectorOnMainThread:@selector(closeWaitDialog) withObject:nil waitUntilDone:NO];
        [self performSelectorOnMainThread:@selector(manageOKRegistration) withObject:nil waitUntilDone:NO];*/

    }

    if( [registrationResultCode isEqualToString:@"KO"] ){
        registrationResult = @"KO";

        //[self performSelectorOnMainThread:@selector(closeWaitDialog) withObject:nil waitUntilDone:NO];
        [self performSelectorOnMainThread:@selector(manageKORegistration) withObject:nil waitUntilDone:YES];
   }
    
    
    //NSLog(@"FINE DOCUMENTO END");
}

-(void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    //NSLog(@"Errore: %i", [parseError code]);
    //[self performSelectorOnMainThread:@selector(closeWaitDialog) withObject:nil waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(manageErrorRegistration) withObject:nil waitUntilDone:YES];

}

-(void) hideAndShowAlerts{
    [newAlert dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *a = [[UIAlertView alloc] initWithTitle:@"Congratulazioni" message:@"Registrazione effettuata con successo" delegate:self cancelButtonTitle:@"cancella" otherButtonTitles:nil, nil];
    [a show];

}

- (void) sendRegistrationData{
    
    NSString *connectionString = [[NSString alloc] initWithFormat:@"%@/photosend/registra_account.php",urlServer];
    
    NSURL *URL = [NSURL URLWithString:connectionString];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    NSString *strParams = [NSString stringWithFormat:@"nome=%@&cognome=%@&cellulare=%@&password=%@",_nomeTextField.text,_cognomeTextField.text, _numeroCellTextField.text,_passwordTextField.text];
    NSString *params = strParams;
    //NSLog(@"STRINGA PHP RILEVATA:%@",strParams);
    //NSString *params = @"";
    NSData *data = [params dataUsingEncoding:NSUTF8StringEncoding];
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%i", [data length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:data];
    
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[[NSOperationQueue alloc] init]
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error)
     {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
         if ([data length] >0 && error == nil && [httpResponse statusCode] == 200)
         {
             //NSLog(@"connessione OK");
             //NSLog([NSString stringWithFormat:@"%@",data]);
             //NSLog([NSString stringWithFormat:@"status code: %i",[httpResponse statusCode]]);
             // DO YOUR WORK HERE
             NSString *strData = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             //NSLog(@"%@",strData);
             NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
             [parser setDelegate:self];
             [parser parse];
             
         }else{
             //NSLog(@"connessione KO");
             //[alertViewWaitRegistration dismiss];
             //[self performSelectorOnMainThread:@selector(closeWaitDialog) withObject:nil waitUntilDone:NO];
             [self performSelectorOnMainThread:@selector(manageConnectionKO) withObject:nil waitUntilDone:YES];
         }
     }];
     }

-(void)closeWaitDialog{
    [alertViewWaitRegistration removeFromSuperview];
    //[alertViewWaitRegistration dismiss];
}

-(void) manageConnectionKO{
    [newAlert dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Impossibile contattare il server" message:@"Impossibile completare l'operazione" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)manageOKRegistration{
    //NSLog(@"Managing OK");

    nomeAccount    = [_nomeTextField text];
    cognomeAccount = [_cognomeTextField text];
    numeroCellulareAccount = [_numeroCellTextField text];
    password = [_passwordTextField text];
    
    [self setSettingsData];
    [self disattivaBottoniELabel];

    [newAlert dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulazioni!" message:@"Registrazione effettuata con successo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    //NSLog(@"alert OK creato");
}

- (void)manageKORegistration{
    
    [newAlert dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registrazione non avvenuta" message:registrationResultCodeDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)manageErrorRegistration{
    
    
    [newAlert dismissWithClickedButtonIndex:0 animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Errore Interno" message:@"Impossibile completare la Registrazione" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    if ([_nomeTextField isFirstResponder] && [touch view] != _nomeTextField) {
        
        [_nomeTextField resignFirstResponder];
    }
    
    if ([_cognomeTextField isFirstResponder] && [touch view] != _cognomeTextField) {
        
        [_cognomeTextField resignFirstResponder];
    }
    
    if ([_numeroCellTextField isFirstResponder] && [touch view] != _numeroCellTextField) {
        
        [_numeroCellTextField resignFirstResponder];
    }
    if ([_passwordTextField isFirstResponder] && [touch view] != _passwordTextField) {
        
        [_passwordTextField resignFirstResponder];
    }
}


- (IBAction)bottoneNuovaRegistrazione:(id)sender {

    CXAlertView *modalViewAskNewRegistration = [[CXAlertView alloc] initWithTitle:@"Conferma" message:@"Vuoi veramente fare una nuova registrazione?" cancelButtonTitle:@"Cancella"];
    
    [modalViewAskNewRegistration addButtonWithTitle:@"OK"
                                         type:CXAlertViewButtonTypeDefault
                                      handler:^(CXAlertView *alertView, CXAlertButtonItem *button) {
                                          [modalViewAskNewRegistration dismiss];
                                      }];
    modalViewAskNewRegistration = [modalViewAskNewRegistration setCXAlertWithCustomColors:modalViewAskNewRegistration];

modalViewAskNewRegistration.didDismissHandler = ^(CXAlertView *modalViewAskTakePhoto) {
    //parte la registrazione
    [self attivaBottoniELabel];
    [self azzeraCampi];
    
    
};
    
 
[modalViewAskNewRegistration show];
    
}
- (void) checkNetworkStatus:(NSString*) hostName{
    //connessione
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [reach currentReachabilityStatus];
    
    //host
    //Reachability *reach = [Reachability reachabilityWithHostName: @"www.apple.com"];
    
    NSString *string;
    switch(status) {
        case NotReachable:
            string = @"Not Reachable";
            isInternetActive_ = NO;
            break;
        case ReachableViaWiFi:
            string = @"Reachable via WiFi";
            isInternetActive_ = YES;
            break;
        case ReachableViaWWAN:
            string = @"Reachable via WWAN";
            isInternetActive_ = YES;
            break;
        default:
            string = @"Unknown";
            isInternetActive_ = NO;
            break;
    }
    
    reach = [Reachability reachabilityWithHostName: hostName];
    switch(status) {
        case NotReachable:
            string = @"Not Reachable";
            isHostActive_ = NO;
            break;
        case ReachableViaWiFi:
            string = @"Reachable via WiFi";
            isHostActive_ = YES;
            break;
        case ReachableViaWWAN:
            string = @"Reachable via WWAN";
            isHostActive_ = YES;
            break;
        default:
            string = @"Unknown";
            isHostActive_ = NO;
            break;
    }
    
}
- (void) goToMainView{

     UIStoryboard *mainStoryboard = self.storyboard;
     UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"primaPagina"];
     
     [self presentViewController:vc animated:YES completion:nil];
    
}

@end
